
import 'package:app/data/common/network/config.dart';
import 'package:app/data/common/network/network_util.dart';
import 'package:app/data/common/network/shared_preference.dart';
import 'package:app/data/services/message_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart' as getx;

enum DioMethod {
  post,
  get,
  delete,
  put,
}

class HttpClient {
  static final HttpClient _singleton = HttpClient._internal();

  factory HttpClient() {
    return _singleton;
  }

  HttpClient._internal();

  Dio httpClient = Dio(BaseOptions(
    connectTimeout: const Duration(seconds: 30),
    sendTimeout: const Duration(seconds: 30),
    receiveTimeout: const Duration(seconds: 30),
  ));

  void initDio() {
    httpClient.options.baseUrl = NetworkConfig.baseAPI;
    httpClient.interceptors.add(
      LogInterceptor(
        responseBody: true,
        requestBody: true,
      ),
    );
    httpClient.interceptors.add(InterceptorsWrapper(
      onRequest: (requestOptions, handler) async {
        final token = await _getToken();
        if (token.isNotEmpty) {
          debugPrint('Bearer $token');
          requestOptions.headers["Authorization"] = 'Bearer $token';
        }
        requestOptions.headers['Connection'] = 'keep-alive';
        return handler.next(requestOptions);
      },
      onError: (error, handler) async {
        if (error.response?.statusCode == 401) {
          // TODO add refresh token
          return handler.resolve(await httpClient.fetch(error.requestOptions));
        }
        if (error.response?.statusCode == 504) {
          if (EasyLoading.isShow) EasyLoading.dismiss();
          var messageService = getx.Get.find<MessageService>();
          messageService.send(Message.error(content: 'timeout_error'.tr));
        }
        return handler.next(error);
      },
    ));
  }

  Future<String> _getToken() async {
    return SharedPreference.shared.getToken();
  }

  Future<dynamic> sendPostRequest(String apiPath, Map<String, dynamic>? body,
      Map<String, dynamic>? param) async {
    if (NetworkUtil().hasConnect) {
      try {
        Map<String, dynamic>? bodys = body;
        final response =
            await httpClient.post(apiPath, data: bodys, queryParameters: param);
        return await _response(response, apiPath, body, DioMethod.post);
      } on DioException catch (error) {
        return _handleError(error, apiPath);
      }
    } else {
      _sendNetworkError();
    }
  }

  Future<dynamic> sendGetRequest(String apiPath, Map<String, dynamic>? param,
      Map<String, dynamic>? data) async {
    if (NetworkUtil().hasConnect) {
      try {
        Map<String, dynamic>? params = param;
        final response =
            await httpClient.get(apiPath, queryParameters: params, data: data);
        return await _response(response, apiPath, param, DioMethod.get);
      } on DioException catch (error) {
        return _handleError(error, apiPath);
      }
    } else {
      _sendNetworkError();
    }
  }

  Future<dynamic> sendPutRequest(
      String apiPath, Map<String, dynamic>? data) async {
    if (NetworkUtil().hasConnect) {
      try {
        Map<String, dynamic>? params = data;
        final response = await httpClient.put(apiPath, data: data);
        return await _response(response, apiPath, params, DioMethod.put);
      } on DioException catch (error) {
        return _handleError(error, apiPath);
      }
    } else {
      _sendNetworkError();
    }
  }

  Future<dynamic> sendDeleteRequest(
      String apiPath, Map<String, dynamic>? param) async {
    if (NetworkUtil().hasConnect) {
      try {
        Map<String, dynamic>? params = param;
        final response =
            await httpClient.delete(apiPath, queryParameters: params);
        return await _response(response, apiPath, param, DioMethod.delete);
      } on DioException catch (error) {
        return _handleError(error, apiPath);
      }
    } else {
      _sendNetworkError();
    }
  }

  Future<dynamic> uploadFile(String apiPath, String filePath) async {
    if (NetworkUtil().hasConnect) {
      try {
        httpClient.options.contentType = "multipart/form-data";
        debugPrint('------------ Request API: $apiPath-------------');
        var formData = FormData.fromMap({
          'avatarFile':
              await MultipartFile.fromFile(filePath, filename: 'upload')
        });
        final response = await httpClient.put(apiPath, data: formData);
        debugPrint(
            '------------  Response api: $apiPath - data: $response ------------');
        httpClient.options.contentType = "application/json";
        return await _response(response, apiPath, null, DioMethod.put);
      } on DioException catch (error) {
        httpClient.options.contentType = "application/json";
        return _handleError(error, apiPath);
      }
    } else {
      _sendNetworkError();
    }
  }

  dynamic _handleError(DioException error, String apiPath) {
    bool e = _checkConnectionError(error);
    int errorCode = -1;
    if (error.response != null) {
      if (e == false && error.response!.statusCode != null) {
        errorCode = error.response!.statusCode!;
      }
    }
    debugPrint('------data: handleError $apiPath $errorCode');

    EasyLoading.dismiss(animation: true);
    return error.response?.data;
  }

  dynamic _response(Response<dynamic> response, String apiPath,
      Map<String, dynamic>? param, DioMethod method) async {
    if (response.data.toString().isEmpty) {
      return response.data;
    }
    return response.data;
  }

  bool _checkConnectionError(e) {
    if (e.toString().contains('SocketException') ||
        e.toString().contains('HandshakeException')) {
      return true;
    } else {
      return false;
    }
  }

  void _sendNetworkError() {
    if (EasyLoading.isShow) EasyLoading.dismiss();
    var messageService = getx.Get.find<MessageService>();
    messageService.send(Message.error(content: 'no_internet_error'.tr));
  }
}
